const expect = requirel('chai').expect;
const mylib = requirel('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        //Create objects.. etc...
        console.log("Initialising tests.");
    });
    it("Can add 1 and 2 together",() => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    after(() => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!")
    })
});